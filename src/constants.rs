pub const SCREEN_WIDTH : i32 = 80;
pub const SCREEN_HEIGHT : i32 = 60;
pub const FPS_LIMIT : i32 = 20;
pub const FONT : &str = "arial10x10.png";

pub const TEXTBOX_HEIGHT : i32 = 10;
pub const MAP_WIDTH : i32 = SCREEN_WIDTH;
pub const MAP_HEIGHT : i32 = SCREEN_HEIGHT - TEXTBOX_HEIGHT;

pub const FLOOR_COLOR : (u8, u8, u8) = (50, 50, 50);
pub const WALL_COLOR : (u8, u8, u8) = (192, 192, 192);
pub const WALL_SYMBOL : char = '#';
pub const FLOOR_SYMBOL : char = '*';

pub const LEAF_MIN_SIZE: i32 = 10;

// Algorithm constants
pub const CELL_AUTO_ITERATIONS : i32 = 5;
pub const CELL_AUTO_WALL_CHANCE : i32 = 50;
pub const CELL_AUTO_FPS : i32 = 1;

pub const DRUNK_FLOORS: i32 = ((MAP_WIDTH as f64 * MAP_HEIGHT as f64) * 0.60) as i32;
pub const DRUNK_FPS :i32 = 60;

pub const TUNNELER_MAX_ROOM_SIZE : i32 = 10;
pub const TUNNELER_MIN_ROOM_SIZE : i32 = 6;
pub const TUNNELER_MAX_ROOMS : i32 = 30;
pub const TUNNELER_FPS : i32 = 30;