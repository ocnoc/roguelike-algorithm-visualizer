use crate::constants::{FLOOR_COLOR, WALL_COLOR, MAP_HEIGHT, MAP_WIDTH, FLOOR_SYMBOL, WALL_SYMBOL};

#[derive(Copy, Clone,Debug)]
pub struct Tile {
    pub color: (u8, u8, u8),
    pub symbol: char
}
#[derive(Debug)]
pub struct Rectangle {
    pub x: i32,
    pub y: i32,
    pub width: i32,
    pub height: i32
}
#[derive(Debug)]
pub struct Map {
    tiles: Vec<Vec<Tile>>,
    pub width: i32,
    pub height: i32
}

impl Tile {
    pub fn new(wall: bool) -> Tile {
        Tile {
            color: if wall { WALL_COLOR } else { FLOOR_COLOR },
            symbol: if wall { WALL_SYMBOL } else {FLOOR_SYMBOL}
        }
    }
    pub fn is_wall(&self) -> bool {
        self.symbol == WALL_SYMBOL
    }
}

impl Map {
    pub fn new(width: i32, height: i32, default_is_wall: bool) -> Map {
        Map {
            tiles: vec![vec![Tile::new(default_is_wall); MAP_HEIGHT as usize]; MAP_WIDTH as usize],
            width,
            height
        }
    }
    pub fn paint_tile(&mut self, wall: bool, x: i32, y: i32) {
        if !self.check_bounds(x, y) {
            return;
        }
        self.tiles[x as usize][y as usize] = Tile::new(wall);
    }
    pub fn get_tiles(&self) -> &Vec<Vec<Tile>> {
        &self.tiles
    }
    pub fn get_tile(&self, x: i32, y: i32) -> Result<&Tile, &str> {
        if !self.check_bounds(x, y) {
            return Err("Location is out of bounds")
        }
        Ok(&self.tiles[x as usize][y as usize])
    }
    fn check_bounds(&self, x: i32, y: i32) -> bool {
        (x >= 0) && (x < self.width) && (y >= 0) && (y < self.height)
    }
    pub fn new_map(&mut self, width: i32, height: i32, walls: bool) {
        *self = Map::new(width, height, walls);
    }
    pub fn replace_map(&mut self, map : Map) {
        *self = map;
    }
    pub fn get_neighbors(&self, x: i32, y: i32) -> [Option<&Tile>;9] {
        let mut neighbors: [Option<&Tile>;9] = [None;9];
        let mut index = 0;
        for i in x-1..x+2 {
            for j in y-1..y+2 {
                neighbors[index] = self.get_tile(i, j).ok();
                index += 1;
            }
        }
        neighbors
    }
    // Paints a given rectangle
    pub fn paint_rect(&mut self, rect: Rectangle, border: i32) {
        let (x, y) = (rect.x, rect.y);
        let (x2, y2) = (rect.x+rect.width, rect.y+rect.height);
        for i in x+border..x2+(1-border) {
            for j in y+border..y2+(1-border) {
                self.paint_tile(false, i, j);
            }
        }
    }
    // Check if a region is all the same tile type
    pub fn check_homogeneous_region(&self, x1: i32, y1: i32, x2 : i32, y2 : i32, wall: bool) -> bool {
        for i in x1..x2+1 {
            for j in y1..y2+1 {
                match self.get_tile(i, j) {
                    Ok(t) => {
                        if (t.is_wall() && !wall) || (!t.is_wall() && wall) {
                            return false;
                        }
                    },
                    Err(_) => {return false},
                }
            }
        }
        true
    }
}

impl Rectangle {
    pub fn get_center(&self) -> (i32, i32) {
        let (x2, y2) = self.get_end_points();
        ((self.x+x2) / 2, (self.y+y2) / 2 )
    }
    pub fn new(x: i32, y: i32, width: i32, height: i32) -> Rectangle {
        Rectangle {
            x,
            y,
            width,
            height
        }
    }
    pub fn get_end_points(&self) -> (i32, i32) {
        (self.x+self.width, self.y+self.height)
    }
}