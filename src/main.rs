mod constants;
mod algorithms;
mod map;

use tcod::{Console, FontLayout, FontType, BackgroundFlag, Color, TextAlignment};
use tcod::console::{Root, Offscreen};
use tcod::colors;
use tcod::input::{Key, KeyCode};
use crate::map::Map;
use crate::constants::{MAP_WIDTH, MAP_HEIGHT, SCREEN_WIDTH, TEXTBOX_HEIGHT, SCREEN_HEIGHT};
use tcod::console::blit;
use crate::algorithms::AlgorithmType;

fn main() {
    let mut root = Root::initializer()
        .font(constants::FONT, FontLayout::Tcod)
        .font_type(FontType::Greyscale)
        .size(constants::SCREEN_WIDTH, constants::SCREEN_HEIGHT)
        .title("Roguelike Dungeon Algorithm Visualizer")
        .init();
    tcod::system::set_fps(constants::FPS_LIMIT);



    let mut textbox = Offscreen::new(SCREEN_WIDTH, TEXTBOX_HEIGHT);
    let mut con = Offscreen::new(SCREEN_WIDTH, SCREEN_HEIGHT-TEXTBOX_HEIGHT);


    let mut map = Map::new(MAP_WIDTH, MAP_HEIGHT, false);

    let mut algo_package = algorithms::init_algorithm(algorithms::AlgorithmType::NONE, &mut map);

    while !root.window_closed() {
        root.clear();
        draw_map(&mut con, &map);
        blit(
            &mut con,
            (0,0),
            (MAP_WIDTH, MAP_HEIGHT),
            &mut root,
            (0,0),
            1.0,
            1.0
        );
        render_text(&mut textbox);
        blit(
            &mut textbox,
            (0,0),
            (SCREEN_WIDTH, TEXTBOX_HEIGHT),
            &mut root,
            (0,SCREEN_HEIGHT-TEXTBOX_HEIGHT),
            1.0,
            1.0
        );
        root.flush();
        let exit = handle_input(&mut root, &mut map, &mut algo_package);
        if exit {
            break;
        }
    }
}

fn handle_input(console: &mut Root, map: &mut Map, package: &mut Option<Box<dyn algorithms::DungeonAlgorithm>>) -> bool {
    if package.is_some() {
        if let Some(key) = console.check_for_keypress(tcod::input::KEY_PRESSED) {
            if key.code == KeyCode::Escape {
                *package = algorithms::init_algorithm(AlgorithmType::NONE, map);
                return false;
            }
        }
        let package_unwrapped = package.as_mut().unwrap();
        let done = package_unwrapped.iterate(map);
        if done {
            *package = algorithms::init_algorithm(AlgorithmType::NONE, map);
        }
        return false;
    }
    let key = console.wait_for_keypress(true);
    match key {
        Key {code: KeyCode::Number0,..} => map.new_map(MAP_WIDTH, MAP_HEIGHT, false),
        Key {code: KeyCode::Number1,..} => *package = algorithms::init_algorithm(AlgorithmType::CELLAUTO, map),
        Key {code: KeyCode::Number2,..} => *package = algorithms::init_algorithm(AlgorithmType::DRUNK, map),
        Key {code: KeyCode::Number3,..} => *package = algorithms::init_algorithm(AlgorithmType::TUNNELING, map),
        Key {code: KeyCode::Escape, ..} => return true,
        _ => {}
    }
    if package.is_some() {
        package.as_mut().unwrap().init(map);
    }
    false
}

fn draw_map(console: &mut Offscreen, map: &Map) {
    let tiles = map.get_tiles();
    for i in 0..map.width {
        for j in 0..map.height {
            let (r, g, b) = tiles[i as usize][j as usize].color;
            console.set_default_foreground(Color::new(r, g, b));
            console.put_char(i, j, tiles[i as usize][j as usize].symbol, BackgroundFlag::None)
        }
    }
}

fn render_text(textbox : &mut Offscreen) {
    const COLUMN_SIZE : i32 = 25;
    const ITEMS_PER_COLUMN : i32 = 9;
    textbox.set_default_foreground(colors::WHITE);
    textbox.set_default_background(colors::BLACK);
    textbox.clear();
    let (mut x, mut y) = (2, 1);
    for string in &algorithms::get_algorithms() {
        if y > ITEMS_PER_COLUMN {
            y = 0;
            x += COLUMN_SIZE
        }
        let msg = String::from(format!("{}", string));
        textbox.print_ex(x, y, BackgroundFlag::None, TextAlignment::Left, msg);
        y += 1;
    }
}

pub fn set_fps(fps: i32) {
    tcod::system::set_fps(fps);
}
