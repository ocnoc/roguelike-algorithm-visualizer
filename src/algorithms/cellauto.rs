use crate::Map;
use rand::Rng;
use crate::constants;
use crate::constants::{MAP_WIDTH, MAP_HEIGHT};
use crate::algorithms::DungeonAlgorithm;

pub struct CellAuto {
    pub iterations: i32
}

impl DungeonAlgorithm for CellAuto {
    fn iterate(&mut self, map: &mut Map) -> bool {
        let mut new_map = Map::new(map.width, map.height, false);
        let mut wall_count ;
        for i in 0..map.width {
            for j in 0..map.height {
                wall_count = 0;
                let neighbors = map.get_neighbors(i, j);
                for tile in &neighbors {
                    if let Some(t) = tile {
                        if t.is_wall() {
                            wall_count += 1
                        }
                    }
                    //match tile {
                     //   Some(t) => {if t.is_wall() { wall_count += 1}},
                     //   None => {}
                    //}
                }
                if wall_count >= 5  {
                    new_map.paint_tile(true, i, j);
                }
                else {
                    new_map.paint_tile(false, i, j);
                }
            }
        }
        self.iterations -= 1;
        map.replace_map(new_map);
        if self.iterations <= 0 { return true; } else { return false; };
    }

    fn init(&mut self, map: &mut Map) {
        self.iterations = constants::CELL_AUTO_ITERATIONS;
        map.new_map(MAP_WIDTH, MAP_HEIGHT, false);
        for i in 0..map.width {
            for j in 0..map.height {
                if rand::thread_rng().gen_range(0, 101) < constants::CELL_AUTO_WALL_CHANCE {
                    map.paint_tile(true, i, j);
                }
            }
        }
    }
}