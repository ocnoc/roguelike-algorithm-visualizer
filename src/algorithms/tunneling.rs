use crate::map::{Map, Rectangle};
use std::cmp::{min, max};
use crate::constants::{MAP_WIDTH, MAP_HEIGHT, TUNNELER_MIN_ROOM_SIZE, TUNNELER_MAX_ROOM_SIZE, TUNNELER_MAX_ROOMS};
use rand::Rng;
use crate::algorithms::DungeonAlgorithm;

pub struct Tunneling {
    pub room_count: i32,
    pub lr_center_x: i32,
    pub lr_center_y: i32
}

impl DungeonAlgorithm for Tunneling {
    fn iterate(&mut self, map: &mut Map) -> bool {
        if self.room_count >= TUNNELER_MAX_ROOMS {
            return true;
        }
        self.room_count += 1;
        let (x, y) = (rand::thread_rng().gen_range(0, MAP_WIDTH), rand::thread_rng().gen_range(0, MAP_HEIGHT));
        let (width, height) = (rand::thread_rng().gen_range(
            TUNNELER_MIN_ROOM_SIZE,
            TUNNELER_MAX_ROOM_SIZE
        ),rand::thread_rng().gen_range(
            TUNNELER_MIN_ROOM_SIZE,
            TUNNELER_MAX_ROOM_SIZE
        ));
        let room = Rectangle {x, y, width, height };
        let (centerx, centery) = room.get_center();
        if map.check_homogeneous_region(x, y, x+width, y+height, true) {
            map.paint_rect(room, 1);
        }
        else {
            return false;
        }
        if (self.lr_center_x != -1) && (self.lr_center_y != -1) {
            let (prev_cx, prev_cy) = (self.lr_center_x, self.lr_center_y);
            let mut rng = rand::thread_rng();
            if rng.gen_bool(1.0 / 2.0) {
                draw_h_tunnel(map, prev_cx, centerx, prev_cy);
                draw_v_tunnel(map, prev_cy, centery, centerx);
            }
            else {
                draw_v_tunnel(map, prev_cy, centery, prev_cx);
                draw_h_tunnel(map, prev_cx, centerx, centery);
            }
        }
        self.lr_center_x = centerx;
        self.lr_center_y = centery;
        false
    }

    fn init(&mut self, map: &mut Map) {
        map.new_map(MAP_WIDTH, MAP_HEIGHT, true);
        self.room_count = 0;
        self.lr_center_x = -1;
        self.lr_center_y = -1;
    }
}

fn draw_h_tunnel(map: &mut Map, x1: i32, x2: i32, y: i32) {
    let (x1, x2) = (min(x1, x2), max(x1, x2));
    for x in min(x1, x2)..max(x1, x2)+1 {
        map.paint_tile(false, x, y);
    }
}

fn draw_v_tunnel(map: &mut Map, y1: i32, y2: i32, x: i32) {
    let (y1, y2) = (min(y1, y2), max(y1, y2));
    for y in y1..y2+1 {
        map.paint_tile(false, x, y);
    }
}