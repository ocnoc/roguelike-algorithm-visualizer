use crate::algorithms::DungeonAlgorithm;
use crate::map::Map;
use crate::constants::{MAP_WIDTH, MAP_HEIGHT};

pub struct BspTree {
    leaves: Vec<Leaf>
}

#[derive(PartialEq)]
pub struct Leaf {
    x: i32,
    y: i32,
    width: i32,
    height: i32,
}

impl DungeonAlgorithm for BspTree {
    fn iterate(&mut self, map: &mut Map) -> bool {
        false
    }

    fn init(&mut self, map: &mut Map) {
        map.new_map(MAP_WIDTH, MAP_HEIGHT, true);
        self.leaves = Vec::new();
        self.leaves.push(Leaf::new(0, 0, MAP_WIDTH, MAP_HEIGHT));
    }
}

impl BspTree {
    pub fn get_children(&self, node: &Leaf) -> (Option<&Leaf>, Option<&Leaf>) {
        let index = self.leaves.iter().position(|lr| { lr == node });
        if index.is_none() {
            return (None, None);
        }
        let index = index.unwrap();
        (self.leaves.get(2*index+1), self.leaves.get(2*index+2))
    }
}

impl Leaf {
    pub fn new(x: i32, y: i32, width: i32, height: i32) -> Leaf {
        Leaf {
            x,
            y,
            width,
            height
        }
    }
}