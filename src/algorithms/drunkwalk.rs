use crate::map::Map;
use crate::constants::{MAP_WIDTH, MAP_HEIGHT, DRUNK_FLOORS};
use rand::Rng;
use crate::algorithms::DungeonAlgorithm;

pub struct DrunkWalk {
    pub x: i32,
    pub y: i32,
    pub floor_count: i32
}

impl DungeonAlgorithm for DrunkWalk {
    fn iterate(&mut self, map: &mut Map) -> bool {
        if self.floor_count >= DRUNK_FLOORS {
            return true;
        }
        let (mut x, mut y, mut floors) = (self.x, self.y, self.floor_count);

        x += rand::thread_rng().gen_range(-1, 2);
        y += rand::thread_rng().gen_range(-1, 2);
        match map.get_tile(x, y) {
            Ok(tile) => {
                if tile.is_wall() {
                    map.paint_tile(false, x, y);
                    floors += 1;
                }
            },
            Err(_) => {
                return false;
            },
        }

        self.x = x;
        self.y = y;
        self.floor_count = floors;
        false
    }

    fn init(&mut self, map: &mut Map) {
        map.new_map(MAP_WIDTH, MAP_HEIGHT, true);
        let x = rand::thread_rng().gen_range(0, MAP_WIDTH);
        let y = rand::thread_rng().gen_range(0, MAP_HEIGHT);
        map.paint_tile(false, x, y);
        self.x = x;
        self.y = y;
        self.floor_count = 1;
    }
}