mod tunneling;
mod cellauto;
mod drunkwalk;
mod bsptree;

use crate::constants;
use crate::map::Map;
use crate::algorithms::cellauto::CellAuto;
use crate::algorithms::drunkwalk::DrunkWalk;
use crate::algorithms::tunneling::Tunneling;

#[derive(PartialEq)]
pub enum AlgorithmType {
    NONE,
    CELLAUTO,
    DRUNK,
    TUNNELING
}

pub trait DungeonAlgorithm {
    fn iterate(&mut self, map : &mut Map) -> bool;
    fn init(&mut self, map: &mut Map);
}

pub fn get_algorithms() -> Vec<String> {
    let mut algos : Vec<String> = Vec::new();
    algos.push(String::from("0) Reset"));
    algos.push(String::from("1) Cellular Automata"));
    algos.push(String::from("2) Drunk Walk"));
    algos.push(String::from("3) Tunneling"));
    algos
}

pub fn init_algorithm(class: AlgorithmType, _map : &mut Map) -> Option<Box<dyn DungeonAlgorithm>> {
    match class {
        AlgorithmType::NONE => {None},
        AlgorithmType::CELLAUTO => {
            crate::set_fps(constants::CELL_AUTO_FPS);
            Some(Box::new(CellAuto {iterations: 0}))
        },
        AlgorithmType::DRUNK => {
            crate::set_fps(constants::DRUNK_FPS);
            Some(Box::new(DrunkWalk {
                x: 0,
                y: 0,
                floor_count: 0
            }))
        },
        AlgorithmType::TUNNELING => {
            crate::set_fps(constants::TUNNELER_FPS);
            Some(Box::new(Tunneling {
                room_count: 0,
                lr_center_x: 0,
                lr_center_y: 0
            }))
        }
    }
}